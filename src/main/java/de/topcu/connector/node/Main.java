package de.topcu.connector.node;

public class Main {

    public static void main(String... args) {
        int i = 10, j = 20;

        Node<Integer> node = new Node<>(), node1 = new Node<>();

        node.setContent(i);
        node1.setContent(j);
        node.setNextNode(node1);

        System.out.println("First Node: " + node.getContent());
        System.out.println("Second Node: " + node.getContent());

    }

}
