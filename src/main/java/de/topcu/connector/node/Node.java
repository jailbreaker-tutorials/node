package de.topcu.connector.node;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Node<T> {

    private T content;
    private Node<T> nextNode;
}
